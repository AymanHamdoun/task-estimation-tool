mod task_estimation;
mod arg_handler;

fn main () {
    match arg_handler::ConsoleOptions::new() {
        Ok(t) => {
            let estimation_params = task_estimation::FormulaParams::new(t.estimates[0], t.estimates[1], t.estimates[2]);
            println!("{}", estimation_params.message(t.time_unit));
        },
        Err(e) => println!("{}", e)
    }
}
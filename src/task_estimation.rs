/*
    Variables:
    ----------
    O => optimistic estimate
    N => nominal estimate
    P => pessimistic estimate

    Formulas:
    ---------
    1. estimate = (o + 4n + p) / 6
    2. variance = (p - o) / 6

    Example:
    ------------
    Let's say I have a task which i think i can finish up in:
    O = 2 days
    N = 3 days
    P = 10 days

    The formulas above will help me decide on an estimate which is 4 days
    However, that alone is not enough, as some unexpected obstacles may appear. Which
    is why the second formula helps estimating the variance which in our case is 1.33 days

    Reference:
    https://codingjourneyman.com/2014/10/06/the-clean-coder-estimation/
*/
pub struct FormulaParams {
    optimistic_estimate: f64,
    nominal_estimate: f64,
    pessimistic_estimate: f64
}

impl FormulaParams {
    pub fn new (optimistic: f64, nominal: f64, pessimistic: f64) -> FormulaParams {
        return FormulaParams {
            optimistic_estimate: optimistic,
            nominal_estimate: nominal,
            pessimistic_estimate: pessimistic
        };
    }

    pub fn estimate (&self) -> f64 {
        return (self.optimistic_estimate + (4.0 * self.nominal_estimate) + self.pessimistic_estimate) / 6.0;
    }

    pub fn variance (&self) -> f64 {
        return (self.pessimistic_estimate - self.optimistic_estimate) / 6.0;
    }

    pub fn message (&self, time_unit: String) -> String {
        let border = "\n**************************************************************\n";
        return format!("\n{}* The task should take around: {:.2} {} with {:.2} variance *{}", border, self.estimate(), time_unit, self.variance(), border);
    }
}
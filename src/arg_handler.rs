const DEFAULT_TIME_UNIT: &str = "hours";

// The Returned End Result after parsing console input:
pub struct ConsoleOptions {
    pub estimates: Vec<f64>,
    pub time_unit: String
}

impl ConsoleOptions {
    /*
       Returns Either an error due to improper command line args or a ConsoleOptions struct from the args
   */
    pub fn new() -> Result<ConsoleOptions, &'static str> {
        // Get passed args from command line:
        let args: Vec<String> = std::env::args().collect();

        // Throw error in case the program was not given all 3 params (4 including the default executable path)
        if args.len() < 4 {
            return Err("Please supply an optimistic, nominal and pessimistic estimates")
        }

        let mut estimates: Vec<f64> = Vec::new();
        let mut time_unit: String = DEFAULT_TIME_UNIT.to_string();

        // Loops over string args passed in command line, categorized and parses each depending on it's order
        for (index, arg) in args.iter().enumerate() {
            match index {
                // 0 => default path to executable which I don't care about
                // 1,2,3 => args representing estimate values:
                1..=3 => {
                    match arg.parse::<f64>() {
                        Ok(t) => estimates.push(t),
                        Err(_e) => return Err("Please make sure your estimates are proper numbers")
                    }
                }
                // optional time unit argument:
                4 => time_unit = arg.to_string(),
                _ => {}
            }
        }
        // Sort estimates so that optimistic is first, nominal is second and pessimistic is last
        estimates.sort_by(|a, b| a.partial_cmp(b).unwrap());

        return Ok(ConsoleOptions {
            estimates,
            time_unit
        });
    }
}
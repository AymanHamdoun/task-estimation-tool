# Task Estimation

This is a command line tool written in [rust](https://www.rust-lang.org/) that estimates how much time a task will take based on a 
formula provided in the book titles "The Clean Coder" and can be found in this article: https://codingjourneyman.com/2014/10/06/the-clean-coder-estimation/.

## Installation

- Install Rust if it is not installed: 
    
    https://www.rust-lang.org/tools/install
     
- Clone the repository

    ```git clone https://gitlab.com/AymanHamdoun/task-estimation-tool.git```

- Navigate to the project 

    ```cd /path/to/project```

- Build the project 

    ```cargo build```
    
- Add Binary Location to your PATH

    ```export PATH=/path/to/repo/target/debug/estimate:$PATH```


## Usage
### Input
- Optimistic Estimate
    - required
- Nominal Estimate
    - required
- Pessimistic Estimate
    - required
- Time unit 
    - optional
    - default: 'hours' 

(Order does not matter since logically optimistic will be the least, pessimistic will be the greatest and the nominal will be in between)

### Example
console inputs: 

```estimate 2 3 10```

```estimate 2 3 10 days```

outputs:

```The task should take around: 4.00 hours with 1.33 variance```

```The task should take around: 4.00 days with 1.33 variance```
